﻿using MoviitTest.CS;
using System.Net.Http.Formatting;
using System.Threading;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;

namespace MoviitTest
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            new Thread(new ThreadStart(Orchestator.Instance.SearchOrders)).Start();
        }
    }
}
