﻿angular.module('MainApp.ui', [])
	.controller('SideNavCtrl', function ($scope, $timeout, $mdSidenav, $log) {

		$scope.toggleLeft = buildToggler('left');

		function buildToggler(componentId) {
			return function () {
				$mdSidenav(componentId).toggle();
			};
		}
	});