﻿angular.module('MainApp.service', [])
	.service('$appService', function ($http) {
		var dataService = {};
		var urlApi = "/api/";

		dataService.getSandwiches = function () {
			return $http.get(urlApi+"sandwich");
		};
		dataService.getSandwich = function (Id) {
			return $http.get(urlApi+"sandwich/" + Id);
		};
		dataService.postSandwich = function (data) {
			return $http.post(urlApi + "sandwich", data);
		};
		dataService.putSandwich = function (data) {
			return $http.put(urlApi + "sandwich/" + data.Id , data);
		};
		dataService.delSandwich = function (data) {
			return $http.delete(urlApi + "sandwich/" + data);
		};


		dataService.getIngredients = function () {
			return $http.get(urlApi+"ingredient");
		};
		dataService.postIngredient = function (data) {
			return $http.post(urlApi + "ingredient", data);
		};
		dataService.delIngredient = function (data) {
			return $http.delete(urlApi + "ingredient/" + data);
		};

		dataService.getClientsOrders = function () {
			return $http.get(urlApi + "client");
		};

		dataService.createClient = function () {
			return $http.get(urlApi + "client/newClient");
		};

		dataService.getOrders = function () {
			return $http.get(urlApi+"order");
		};
		dataService.ordersByClient = function(){
			return $http.post(urlApi + "order/byClient", getClient());
		};
		dataService.ordersByClientId = function(id){
			return $http.post(urlApi + "order/byClient", {Id:id});
		};
		dataService.reportOrders = function () {
			return $http.post(urlApi + "order/reportOrders");
		};
		dataService.getOrder = function (Id) {
			return $http.get(urlApi+"order/" + Id);
		};
		dataService.getOrderDetail = function (Id) {
			return $http.get(urlApi + "order/detail/" + Id);
		};
		dataService.postOrder = function (data) {
			return $http.post(urlApi + "order", data);
		};
		dataService.putOrder = function (data) {
			return $http.put(urlApi + "order/" + data.Id , data);
		};
		dataService.orderConfirm = function(data){
			return $http.put(urlApi + "order/confirm" , data);	
		}
		dataService.delOrder = function (data) {
			return $http.delete(urlApi + "order/" + data);
		};
		dataService.postPayment = function (data) {
			return $http.post(urlApi + "payment", data);
		};
		return dataService;
	});