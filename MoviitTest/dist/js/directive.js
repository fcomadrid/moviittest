﻿angular.module("MainApp.directive", [])
	.directive("sidenavMenu", function () {
		return {
			templateUrl: getPartials("layout/sidenav"),
			replace: true
		};
	});