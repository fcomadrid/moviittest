function dialogAlert($mdDialog, title, message){
	return $mdDialog.alert()
		.title(title)
		.textContent(message)
		.clickOutsideToClose(false)
		.ok('OK');	
}
function getClient(){
	try{
		return JSON.parse(localStorage.getItem('client'));
	}
	catch{
		return null;
	}
}