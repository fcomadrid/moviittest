var salsaSide = [
    'On Top',
    'Inside',
    'No Salsa'
];
var cut = [
    'Square',
    'Round',
    'Triangle'
];

angular.module('MainApp.controller', [])
    .controller("sandwichCtrl", function($scope, $appService) {
        $scope.del = function(id) {
            if (confirm("Esta seguro de eliminarlo?")) {
                $appService.delSandwich(id);
                getSandwiches();
            }
        };
        getSandwiches = function() {
            $appService.getSandwiches().then(function(resp) {
                $scope.sandwiches = resp.data;
            }, function(error) {
                console.log(error);
            });
        };
        getSandwiches();


    })
    .controller("sandwichAddCtrl", function($scope, $appService, $mdDialog, $state) {
        $scope.save = function() {
            $appService.postSandwich($scope.sandwich).then(
                function(resp) {
                    $mdDialog.show(
                        dialogAlert($mdDialog, "Accion", "Creado con exito")
                    ).then(function() {
                        $state.go("sandwich.list");
                    });
                },
                function(error) {
                    $mdDialog.show(
                        dialogAlert($mdDialog, "Accion", "Error al crear sandwich")
                    );
                });
        };
        $appService.getIngredients().then(function(resp) {
            $scope.ingredients = resp.data;

        }, function(error) {

        });
        $scope.salsaSide = salsaSide;
        $scope.cuts = cut;
    })
    .controller("sandwichEditCtrl", function($scope, $appService, $mdDialog, $state, $stateParams) {
        $scope.sandwich = {};
        var ingredientsId = [];
        $appService.getSandwich($stateParams.Id)
            .then(function(resp) {
                $scope.sandwich = resp.data;
                angular.forEach($scope.sandwich.Ingredients, function(value) {
                    ingredientsId.push(value.Id);
                });
                $scope.sandwich.Ingredients = ingredientsId;
            }, function(error) {
                $mdDialog.show(
                    dialogAlert($mdDialog, "Accion", "Error al cargar sandwich favor reintentar")
                ).then(function() {
                    $state.go("sandwich.list");
                });
            });

        $scope.save = function() {
            $appService.putSandwich($scope.sandwich)
                .then(function(resp) {
                    $mdDialog.show(
                        dialogAlert($mdDialog, "Accion", "Actualizado con exito")
                    ).then(function() {
                        $state.go("sandwich.list");
                    });
                }, function(error) {
                    $mdDialog.show(
                        dialogAlert($mdDialog, "Accion", "Error al editar")
                    );
                });
        };
        $appService.getIngredients().then(function(resp) {
            $scope.ingredients = resp.data;

        }, function(error) {
            console.log(error);
        });
        $scope.salsaSide = salsaSide;
        $scope.cuts = cut;
    })

    .controller("ingredientCtrl", function($scope, $appService) {

        $scope.del = function(id) {
            if (confirm("Esta seguro de eliminarlo?")) {
                $appService.delIngredient(id).then(function(resp) {
                    getIngredients();
                });
            }
        };
        getIngredients = function() {
            $appService.getIngredients().then(function(resp) {
                $scope.ingredients = resp.data;
            }, function(error) {
                console.log(error);
            });
        };
        getIngredients();

    })
    .controller("ingredientAddCtrl", function($scope, $appService, $state, $mdDialog) {
        $scope.save = function() {
            $appService.postIngredient($scope.ingredient).then(function() {
                $mdDialog.show(
                    dialogAlert($mdDialog, "Accion", "Creado con exito")
                ).then(function() {
                    $state.go("ingredient.list");
                });
            }, function(error) {
                $mdDialog.show(
                    dialogAlert($mdDialog, "Accion", "Error al crear ingrediente")
                );
            });
        };
    })
    .controller("adminReportCtrl", function($scope, $appService) {
        $scope.report = [];
        $appService.reportOrders().then(function(resp) {
            $scope.report = resp.data;
        }, function(error) {

        });
    })
    .controller("clientCtrl", function($rootScope, $scope, $appService, $state, $mdDialog) {
        $scope.Orders = {};
        $scope.Orders.client = getClient();
        $scope.Orders.Sandwiches = [];
        $scope.add = function(id) {
            var index = $scope.Orders.Sandwiches.indexOf(id);
            if (index < 0) {
                id.Quantity = 1;
                $scope.Orders.Sandwiches.push(id);
            } else {
                $scope.Orders.Sandwiches[index].Quantity += 1;

            }

        };
        $scope.remove = function(s) {
            var index = $scope.Orders.Sandwiches.indexOf(s);
            $scope.Orders.Sandwiches.splice(index, 1);
        };
        $scope.total = 0;
        $scope.currencyAmount = 1;
        $scope.currency = 'CLP';
        $scope.changeCurrency = function() {

        };
        $appService.getSandwiches().then(function(resp) {
            $scope.sandwiches = resp.data;
        }, function(error) {
            console.log(error);
        });
        $scope.$watch("currency", function(value) {
            if (value === 'CLP') {
                $scope.currencyAmount = 1;
            } else {
                $scope.currencyAmount = $rootScope.dolarPrice;
            }
        });

        $scope.$watch(function($scope) {
            return $scope.Orders.Sandwiches;
        }, function(newVal) {
            $scope.total = 0;
            angular.forEach(newVal, function(value) {
                $scope.total += value.Quantity * value.Price;
            });
        }, true);


        $scope.setOrder = function() {
            $scope.Orders.total = $scope.total;
            $appService.postOrder($scope.Orders).then(
                function(resp) {

                    $mdDialog.show(dialogAlert($mdDialog, "Pedido", "Pedido N°" + resp.data.Id + " Creado"))
                        .then(function() {
                            $state.go("client.orderDetail");
                        });

                },
                function(error) {

                });
        };
    })
    .controller('clientOrderDetailCtrl', function($scope, $appService, $mdDialog) {
        $scope.orders = [];
        var sandwiches = [];

        getOrders = function() {
            $appService.ordersByClient().then(function(resp) {
                $scope.orders = resp.data;
            }, function(error) {});
        }
        $scope.Confirm = function(order) {
            $appService.orderConfirm(order).then(function(resp) {
                $mdDialog.show(dialogAlert($mdDialog, "Pedido", "Pedido Confirmado"))
                    .then(function() {});
                getOrders();
            }, function(error) {

            })
        };
        getOrders();

    })
    .controller('clientOrderEditCtrl', function($rootScope, $scope, $state, $appService, $stateParams, $mdDialog) {
        $scope.Orders = {};
        var sandwiches = [];
        $appService.getSandwiches().then(function(resp) {
            $scope.sandwiches = resp.data;
        }, function(error) {
            console.log(error);
        });

        $appService.getOrder($stateParams.Id).then(function(resp) {
            $scope.Orders = resp.data;
            var count = $scope.Orders.Sandwiches.length;
            angular.forEach($scope.Orders.Sandwiches, function(value) {
                count--;
                $appService.getSandwich(value.Id).then(function(resp) {
                    resp.data.Quantity = value.Quantity;
                    sandwiches.push(resp.data);
                }, function(error) {

                });
                if (count === 0) {
                    $scope.Orders.Sandwiches = sandwiches;
                }
            });

        }, function(error) {});
        $scope.add = function(id) {
            var index = $scope.Orders.Sandwiches.indexOf(id);
            if (index < 0) {
                id.Quantity = 1;
                $scope.Orders.Sandwiches.push(id);
            } else {
                $scope.Orders.Sandwiches[index].Quantity += 1;

            }

        };
        $scope.remove = function(s) {
            var index = $scope.Orders.Sandwiches.indexOf(s);
            $scope.Orders.Sandwiches.splice(index, 1);
        };
        $scope.total = 0;
        $scope.currencyAmount = 1;
        $scope.currency = 'CLP';
        $scope.changeCurrency = function() {

        };

        $scope.$watch("currency", function(value) {
            if (value === 'CLP') {
                $scope.currencyAmount = 1;
            } else {
                $scope.currencyAmount = $rootScope.dolarPrice;
            }
        });

        $scope.$watch(function($scope) {
            return $scope.Orders.Sandwiches;
        }, function(newVal) {
            $scope.total = 0;
            angular.forEach(newVal, function(value) {
                $scope.total += value.Quantity * value.Price;
            });
        }, true);


        $scope.setOrder = function() {
            $scope.Orders.total = $scope.total;
            $appService.putOrder($scope.Orders).then(
                function(resp) {

                    $mdDialog.show(dialogAlert($mdDialog, "Pedido", "Pedido Modificado"))
                        .then(function() {
                            $state.go("client.orderDetail");
                        });
                },
                function(error) {

                });
        };
    })
    .controller('cashierCtrl', function($rootScope, $scope, $appService, $mdDialog) {
        $scope.currencyAmount = 1;
        $scope.currency = 'CLP';
        $scope.orders = [];
        $scope.payments = []
        $scope.clients = []
        $appService.getClientsOrders()
            .then(function(resp) {
                    $scope.client = resp.data;
                    angular.forEach($scope.client, function(value) {
                        $scope.payments.push(getPayment(value));
                    })
                },
                function(error) {});


        getPayment = function(client) {
            var toPay = 0;
            angular.forEach(client.Orders, function(value) {
                toPay += value.Total;
            });
            return {
                Amount: toPay,
                Orders: client.Orders,
                Client: client
            }
        }
        $scope.pay = function(p){
            $appService.postPayment(p).then(function(resp){
                $mdDialog.show(dialogAlert($mdDialog, "Pedido", "Cuenta Pagada"))
                    .then(function() {
                        getOrders();
                }, function(error){

                })   
            }, function(error){

            })
        }

        $scope.$watch("currency", function(value) {
            if (value === 'CLP') {
                $scope.currencyAmount = 1;
            } else {
                $scope.currencyAmount = $rootScope.dolarPrice;
            }
        })


    })



    .controller('cashierDetailCtrl', function($scope, $appService, $stateParams) {
        $scope.orders = [];
        var sandwiches = [];



        getOrders = function() {
            $appService.ordersByClientId($stateParams.Id).then(function(resp) {
                $scope.orders = resp.data;
            }, function(error) {});
        }        
        getOrders();

    })
    .controller('cashierDetailViewCtrl', function($scope, $stateParams, $appService, $rootScope) {
        $scope.Orders = {};
        var sandwiches = [];

        $appService.getOrder($stateParams.Id).then(function(resp) {
            $scope.Orders = resp.data;
            var count = $scope.Orders.Sandwiches.length;
            angular.forEach($scope.Orders.Sandwiches, function(value) {
                count--;
                $appService.getSandwich(value.Id).then(function(resp) {
                    resp.data.Quantity = value.Quantity;
                    sandwiches.push(resp.data);
                }, function(error) {

                });
                if (count === 0) {
                    $scope.Orders.Sandwiches = sandwiches;
                }
            });

        }, function(error) {});        
        $scope.total = 0;
        $scope.currencyAmount = 1;
        $scope.currency = 'CLP';
        $scope.changeCurrency = function() {

        };

        $scope.$watch("currency", function(value) {
            if (value === 'CLP') {
                $scope.currencyAmount = 1;
            } else {
                $scope.currencyAmount = $rootScope.dolarPrice;
            }
        });

        $scope.$watch(function($scope) {
            return $scope.Orders.Sandwiches;
        }, function(newVal) {
            $scope.total = 0;
            angular.forEach(newVal, function(value) {
                $scope.total += value.Quantity * value.Price;
            });
        }, true);

    });