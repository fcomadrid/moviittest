﻿var getPartials = function (str) {
	return "dist/partials/" + str + ".html";
};

var app = angular.module("MainApp", [
	"ui.router",
	"ngMaterial",
	"ngAria",
	"ngAnimate",
	"ngRoute",
	"MainApp.controller",
	"MainApp.directive",
	"MainApp.service",
	"MainApp.ui"
])
	.config(function ($mdThemingProvider) {
		$mdThemingProvider.theme('default')
			.primaryPalette('blue', {
				'default': '500'
			});
	})

	
	.config(['$stateProvider', "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider
			.when('/index', '/');
		$stateProvider
			.state('report',{
				url: "/",
				controller: 'adminReportCtrl',
				templateUrl: getPartials("report/orders")
			})
			.state('sandwich', {
				abstract: true,
				url: "",
				template: '<div ui-view autoscroll="false"/>'
			})
			.state('sandwich.list', {
				url: "/sandwiches",
				templateUrl: getPartials("sandwich/index"),
				controller: "sandwichCtrl"
			})
			.state('sandwich.add', {
				url: "/sandwiches/add",
				templateUrl: getPartials("sandwich/create"),
				controller: "sandwichAddCtrl"
			})
			.state('sandwich.edit', {
				url: "/sandwiches/edit/:Id",
				templateUrl: getPartials("sandwich/edit"),
				controller: "sandwichEditCtrl"
			})
			.state("ingredient", {
				abstract: true,
				url: "",
				template: '<div ui-view autoscroll="false"/>'
			})
			.state('ingredient.list', {
				url: "/ingredient",
				templateUrl: getPartials("ingredient/index"),
				controller: "ingredientCtrl"
			})
			.state('ingredient.add', {
				url: "/ingredient/add",
				templateUrl: getPartials("ingredient/create"),
				controller: "ingredientAddCtrl"
			})
			.state('client', {
				abstract: true,
				url: '',
				template: '<div ui-view autoscroll="false"/>'
			})
			.state('client.order', {
				url: "/client",
				templateUrl: getPartials("client/order"),
				controller: "clientCtrl"
			})
			.state('client.orderDetail', {
				url: "/client/orderDetail",
				templateUrl: getPartials("client/orderDetail"),
				controller: "clientOrderDetailCtrl"
			})
			.state('client.orderEdit', {
				url: "/client/orderEdit/:Id",
				templateUrl: getPartials("client/order"),
				controller: "clientOrderEditCtrl"
			})
			.state('cashier', {
				abstract: true,
				url: '',
				template: '<div ui-view autoscroll="false"/>'
			})
			.state('cashier.order', {
				url: '/cashier/order',
				templateUrl: getPartials("cashier/order"),
				controller: 'cashierCtrl'
			})
			.state('cashier.orderDetail', {
				url: '/cashier/orderDetail/:Id',
				templateUrl: getPartials("cashier/detail"),
				controller: 'cashierDetailCtrl'
			})
			.state('cashier.detailView',{
				url: '/cashier/detailView/:Id',
				templateUrl: getPartials("cashier/detailView"),
				controller: 'cashierDetailViewCtrl'
			});
				
	}])
	.run(function($rootScope, $http){
		var client = getClient();
		$http.get("https://mindicador.cl/api").then(function(resp){
			$rootScope.dolarPrice = resp.data.dolar.valor;
		},function(error){

		});
		if (client === undefined || client === null){
			$http.post("api/client").then(function (resp) {
				client = resp.data;
				localStorage.setItem('client', JSON.stringify(client));
			}, function (error) {

			});

		}

	});