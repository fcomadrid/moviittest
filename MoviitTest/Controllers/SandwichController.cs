﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MoviitTest.Models;
using MoviitTest.Models.ApiModels;

namespace MoviitTest.Controllers
{
    public class SandwichController : ApiController
    {
        private MainContext db = new MainContext();

        // GET: api/Sandwich
        public async Task<IHttpActionResult> GetSandwich()
        {
            return Ok(await db.Sandwich.Include(x => x.Ingredients).ToListAsync());
        }

        // GET: api/Sandwich/5
        [ResponseType(typeof(Sandwich))]
        public async Task<IHttpActionResult> GetSandwich(int id)
        {
            Sandwich sandwich = await db.Sandwich.Include(x => x.Ingredients).FirstAsync(x=> x.Id == id);
            if (sandwich == null)
            {
                return NotFound();
            }

            return Ok(sandwich);
        }

        // PUT: api/Sandwich/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSandwich(int id, SandwichRequest sandwichRequest)
        {            
            Sandwich sandwich = await db.Sandwich.FirstAsync(x => x.Id == id);
            sandwich.Price = sandwichRequest.Price;
            sandwich.Compress = sandwichRequest.Compress;
            sandwich.Cut = sandwichRequest.Cut;
            sandwich.Type = sandwichRequest.Type;            
            sandwich.Ingredients = new List<Ingredient>();
            var ing = (from i in db.Ingredient.ToList() where sandwichRequest.Ingredients.Contains(i.Id) select i).ToList();
            ing.ForEach(x => sandwich.Ingredients.Add(x));
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }            
            if (id != sandwich.Id)
            {
                return BadRequest();
            }
            sandwich.UpdateAt = DateTime.Now;
            db.Entry(sandwich).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SandwichExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sandwich
        [ResponseType(typeof(Sandwich))]
        public async Task<IHttpActionResult> PostSandwich(SandwichRequest sandwichRequest)
        {
            Sandwich sandwich = new Sandwich();
            sandwich = sandwichRequest;
            sandwich.Ingredients = (from i in db.Ingredient.ToList() where sandwichRequest.Ingredients.Contains(i.Id) select i).ToList();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Sandwich.Add(sandwich);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = sandwich.Id }, sandwich);
        }

        // DELETE: api/Sandwich/5
        [ResponseType(typeof(Sandwich))]
        public async Task<IHttpActionResult> DeleteSandwich(int id)
        {
            Sandwich sandwich = await db.Sandwich.FindAsync(id);
            if (sandwich == null)
            {
                return NotFound();
            }

            db.Sandwich.Remove(sandwich);
            await db.SaveChangesAsync();

            return Ok(sandwich);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SandwichExists(int id)
        {
            return db.Sandwich.Count(e => e.Id == id) > 0;
        }
    }
}