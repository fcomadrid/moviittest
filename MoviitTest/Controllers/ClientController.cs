﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MoviitTest.Models;

namespace MoviitTest.Controllers
{
    public class ClientController : ApiController
    {
        private MainContext db = new MainContext();

        // GET: api/Client
        public async Task<IHttpActionResult> GetClient()
        {
            var client = from a in db.Client.ToList()
                         join o in db.Order.ToList() on a.Id equals o.Client.Id
                       where o.Date.Value.ToString("ddMMyyyy") == DateTime.Now.ToString("ddMMyyyy") && o.Status != "Paid" select a;
            return Ok(client.ToList().GroupBy(x => x.Orders).Select(q => q.First()));
        }

        // GET: api/Client/5
        [ResponseType(typeof(Client))]
        public async Task<IHttpActionResult> GetClient(int id)
        {
            Client client = await db.Client.FindAsync(id);
            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }

        // PUT: api/Client/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutClient(int id, Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != client.Id)
            {
                return BadRequest();
            }

            db.Entry(client).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Client
        [ResponseType(typeof(Client))]
        public async Task<IHttpActionResult> PostClient()
        {
            Client client = new Client()
            {
                Name = String.Concat("client_",  Guid.NewGuid().ToString().Replace("-", "") )
            };

            db.Client.Add(client);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = client.Id }, client);
        }

        // DELETE: api/Client/5
        [ResponseType(typeof(Client))]
        public async Task<IHttpActionResult> DeleteClient(int id)
        {
            Client client = await db.Client.FindAsync(id);
            if (client == null)
            {
                return NotFound();
            }

            db.Client.Remove(client);
            await db.SaveChangesAsync();

            return Ok(client);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClientExists(int id)
        {
            return db.Client.Count(e => e.Id == id) > 0;
        }
    }
}