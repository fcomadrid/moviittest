using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MoviitTest.CS;
using MoviitTest.Models;
using MoviitTest.Models.ApiModels;

namespace MoviitTest.Controllers
{
    public class OrderController : ApiController
    {
        private MainContext db = new MainContext();

        // GET: api/Order
        public async Task<IHttpActionResult> GetOrder()
        {
            var c = await db.Order.ToListAsync();
            return Ok(c.FindAll(x => (x.Date.Value.ToString("ddMMyyyy") == DateTime.Now.ToString("ddMMyyyy"))).GroupBy(x => x.Client.Id));
        }
        [ResponseType(typeof(Order))]
        [Route("api/order/byClient")]
        public async Task<IHttpActionResult> OrdersByClient([FromBody] Client client)
        {
            var c = await db.Client.FirstAsync(x => x.Id==client.Id);
            return Ok(c.Orders);
        }

        [Route("api/order/detail/:id")]
        public async Task<IHttpActionResult> GetDetailOrder(int id)
        {
            var c = await db.Order.FirstAsync(x => x.Id == id);
            return Ok(c);
        }

        // GET: api/Order/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> GetOrder(int id)
        {
            Order order = await db.Order.FindAsync(id);
            List<OrderSandwichRequest> orderSandwichRequest = new List<OrderSandwichRequest>();
            order.OrderItems.ForEach(x => orderSandwichRequest.Add(new OrderSandwichRequest() {  Id = x.Sandwich.Id, Quantity = x.Quantity}));
            OrderRequest orderRequest = new OrderRequest()
            {
                Id = order.Id,
                Client = order.Client,
                Sandwiches = orderSandwichRequest,
                Total = order.Total
            };
            if (order == null)
            {
                return NotFound();
            }

            return Ok(orderRequest);
        }
        [Route("api/order/confirm")]
        [HttpPut]
        public async Task<IHttpActionResult> OrderUpdate( Order order)
        {
            Order o = db.Order.Find(order.Id);
            o.Status = "Confirmed";
            db.Entry(o).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
                Orchestator.Instance.SetActive();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(o.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("api/order/ReportOrders")]
        public async Task<IHttpActionResult> ReportOrders()
        {
            DateTime dateCompare = DateTime.Now;
            List<ReportResponse> report = new List<ReportResponse>();
            dateCompare = dateCompare.AddDays(-7);
            var list = (from o in await db.Order.ToListAsync()
                        join oi in await db.OrderItem.ToListAsync() on o.Id equals oi.Order.Id
                        where o.Date.Value.Ticks >= dateCompare.Ticks                        
                        select new {
                            oi.Sandwich,
                            oi.Quantity
                        }).ToList();
                        
            foreach (var r in list)
            {
                ReportResponse rp = new ReportResponse();
                rp.Sandwich = r.Sandwich;
                rp.Count = list.Where(x => x.Sandwich.Id == rp.Sandwich.Id).Sum(x => x.Quantity);
                report.Add(rp);
            }           
            return Ok(report.GroupBy(x => x.Sandwich).Select(q => q.First()));
        }

        // PUT: api/Order/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder(int id, OrderRequest orderRequest)
        {
            db.OrderItem.RemoveRange(await db.OrderItem.Where(x => x.Order.Id == orderRequest.Id).ToListAsync());
            await db.SaveChangesAsync();
            List<OrderItem> orderItems = new List<OrderItem>();
            Client client = await db.Client.FindAsync(orderRequest.Client.Id);
            foreach (var s in orderRequest.Sandwiches)
            {
                Sandwich sandwich = await db.Sandwich.FirstAsync(x => s.Id == x.Id);
                OrderItem orderItem = new OrderItem()
                {
                    Quantity = s.Quantity,
                    Sandwich = sandwich,
                    Total = sandwich.Price * s.Quantity
                };
                orderItems.Add(orderItem);
            }

            Order order = await db.Order.FindAsync(id);

            order.OrderItems = orderItems;
            order.Total = orderRequest.Total;

            if (id != order.Id)
            {
                return BadRequest();
            }


            db.Entry(order).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Order
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostOrder(OrderRequest orderRequest)
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            Client client = await db.Client.FindAsync(orderRequest.Client.Id);
            foreach (var s in orderRequest.Sandwiches)
            {
                Sandwich sandwich = await db.Sandwich.FirstAsync(x => s.Id == x.Id);
                OrderItem orderItem = new OrderItem()
                {
                    Quantity = s.Quantity,
                    Sandwich = sandwich,
                    Total = sandwich.Price * s.Quantity
                };
                orderItems.Add(orderItem);
            }
            Order order = new Order()
            {
                Client = client,
                OrderItems = orderItems,
                Total = orderRequest.Total
            };
            order.Status = "Pending";
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Order.Add(order);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = order.Id }, order);
        }

        // DELETE: api/Order/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> DeleteOrder(int id)
        {
            Order order = await db.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            db.Order.Remove(order);
            await db.SaveChangesAsync();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Order.Count(e => e.Id == id) > 0;
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             