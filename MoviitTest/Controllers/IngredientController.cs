﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MoviitTest.Models;
using MoviitTest.Machine;

namespace MoviitTest.Controllers
{
    public class IngredientController : ApiController
    {
        private MainContext db = new MainContext();

        // GET: api/Ingredient
        public List<Ingredient> GetIngredient()
        {
            return db.Ingredient.Include(x => x.Sandwiches).AsNoTracking().ToList();
        }

        // GET: api/Ingredient/5
        [ResponseType(typeof(Ingredient))]
        public async Task<IHttpActionResult> GetIngredient(int id)
        {
            Ingredient ingredient = await db.Ingredient.Include(x => x.Sandwiches).AsNoTracking().FirstAsync(x=> x.Id == id);
            if (ingredient == null)
            {
                return NotFound();
            }

            return Ok(ingredient);
        }

        // PUT: api/Ingredient/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutIngredient(int id, Ingredient ingredient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ingredient.Id)
            {
                return BadRequest();
            }

            db.Entry(ingredient).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IngredientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ingredient
        [ResponseType(typeof(Ingredient))]
        public async Task<IHttpActionResult> PostIngredient(Ingredient ingredient)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ingredient.Add(ingredient);
            await db.SaveChangesAsync();
            IngredientParse ingredientParse = new IngredientParse();
            db.Ingredient.ToList().ForEach(x => ingredientParse.Ingredients.Add(new IngredientObject() { Id = x.Id, Name = x.Name }));
            Config.Writer(ingredientParse);
            return CreatedAtRoute("DefaultApi", new { id = ingredient.Id }, ingredient);
        }

        // DELETE: api/Ingredient/5
        [ResponseType(typeof(Ingredient))]
        public async Task<IHttpActionResult> DeleteIngredient(int id)
        {
            Ingredient ingredient = await db.Ingredient.FindAsync(id);
            if (ingredient == null)
            {
                return NotFound();
            }

            db.Ingredient.Remove(ingredient);
            await db.SaveChangesAsync();

            IngredientParse ingredientParse = new IngredientParse();
            db.Ingredient.ToList().ForEach(x => ingredientParse.Ingredients.Add(new IngredientObject() { Id = x.Id, Name = x.Name }));
            Config.Writer(ingredientParse);
            return Ok(ingredient);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IngredientExists(int id)
        {
            return db.Ingredient.Count(e => e.Id == id) > 0;
        }
    }
}