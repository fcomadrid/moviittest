﻿using System.Web.Optimization;

namespace MoviitTest
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.min.js",
                        "~/Scripts/angular-aria.min.js",
                        "~/Scripts/angular-animate.min.js",
                        "~/Scripts/angular-route.min.js",
                        "~/Scripts/angular-material.min.js",
                        "~/Scripts/angular-ui-router.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/dist/js/app.js",
                        "~/dist/js/controller.js",
                        "~/dist/js/directive.js",
                        "~/dist/js/service.js",
                        "~/dist/js/ui.js",
                        "~/dist/js/functions.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome.min.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/angular-material.min.css",
                      "~/dist/css/main.css"));
        }
    }
}
