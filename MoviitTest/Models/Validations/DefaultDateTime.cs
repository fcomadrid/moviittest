﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MoviitTest.Models.Validations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DefaultDateTimeValueAttribute : ValidationAttribute
    {
        public DefaultDateTimeValueAttribute()
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo property = validationContext.ObjectType.GetProperty(validationContext.MemberName);

            // Set default value only if no value is already specified 
            if (value == null)
            {
                DateTime defaultValue = DateTime.Now;
                property.SetValue(validationContext.ObjectInstance, defaultValue);
            }

            return ValidationResult.Success;
        }
    }
}