﻿namespace MoviitTest.Models
{
    public class OrderItem
    {
        public int Id { get; set; }        
        public int Quantity { get; set; }
        public decimal Total { get; set; }
        public virtual Order Order { get; set; }
        public virtual Sandwich Sandwich { get; set; }
    }
}