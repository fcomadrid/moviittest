﻿using System.Collections.Generic;

namespace MoviitTest.Models
{
    public class Client
    {
        public Client()
        {
            Orders = new List<Order>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Order> Orders { get; set; }
    }
}