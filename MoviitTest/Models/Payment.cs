﻿using MoviitTest.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models
{
    public class Payment
    {
        public Payment()
        {
            Orders = new List<Order>();
        }
        public int Id { get; set; }
        [Required]
        public decimal Amount { get; set; }        
        [DefaultDateTimeValue]
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt {get;set;}
        [DefaultValue("Pending")]
        public string Status { get; set; }
        public virtual List<Order> Orders { get; set; }
    }
}