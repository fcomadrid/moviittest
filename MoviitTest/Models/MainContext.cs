namespace MoviitTest.Models
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public partial class MainContext : DbContext
    {
        public DbSet<Client> Client { get; set; }
        public DbSet<Sandwich> Sandwich { get; set; }
        public DbSet<Ingredient> Ingredient { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public MainContext()
            : base("name=MainContext")
        {
        }


        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Conventions.Remove<PluralizingTableNameConvention>();
            builder.Entity<Sandwich>()
                .HasMany<Ingredient>(s => s.Ingredients)
                .WithMany(s => s.Sandwiches)
                .Map(s => s.MapLeftKey("IngredientId").MapRightKey("SandwichId").ToTable("SandwichIngredient"));
            base.OnModelCreating(builder);            
        }
    }
}
