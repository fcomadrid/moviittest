﻿using MoviitTest.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models
{
    public class Order
    {
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
        public int Id { get; set; }        
        [DefaultDateTimeValue]
        public DateTime? Date { get;set; }        
        [DefaultValue("Pending")]
        public String Status { get; set; }
        public decimal Total { get; set; }
        [Required]
        public virtual Client Client { get; set; }
        public virtual List<OrderItem> OrderItems { get; set; }
        public virtual Payment Payment { get; set; }
    }
}