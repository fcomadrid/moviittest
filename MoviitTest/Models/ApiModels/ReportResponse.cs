﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviitTest.Models.ApiModels
{
    public class ReportResponse
    {
        public Sandwich Sandwich { get; set; }
        public int Count { get; set; }
    }
}