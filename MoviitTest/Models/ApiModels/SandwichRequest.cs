﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models.ApiModels
{
    public class SandwichRequest
    {
        public SandwichRequest() {
            Ingredients = new List<int>();
        }
        public int Id { get; set; }
        public bool Compress { get; set; }
        [Required]
        public string Cut { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Salsa { get; set; }
        [Required]
        public decimal Price { get; set; }
        public List<int> Ingredients { get; set; }
        public static implicit operator Sandwich(SandwichRequest s)
        {
            return new Sandwich()
            {
                Compress = s.Compress,
                Cut = s.Cut,
                Type = s.Type,
                Salsa = s.Salsa,
                Price = s.Price
            };
        }
    }
    
}