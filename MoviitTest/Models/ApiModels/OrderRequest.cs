﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models.ApiModels
{
    public class OrderRequest
    {
        public int Id { get; set; }
        [Required]
        public Client Client { get; set; }
        [Required]
        public List<OrderSandwichRequest> Sandwiches { get; set; }
        [Required]
        public decimal Total { get; set; }
    }
    public class OrderSandwichRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}