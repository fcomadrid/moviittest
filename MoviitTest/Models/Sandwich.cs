﻿using MoviitTest.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models
{
    public class Sandwich
    {
        public Sandwich() {
            Ingredients = new List<Ingredient>();
            Orders = new List<OrderItem>();
        }
        public int Id { get; set; }        
        public bool Compress { get; set; }
        [Required]
        public string Cut { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Salsa { get; set; }
        [Required]
        public decimal Price { get; set; }
        public List<Ingredient> Ingredients { get; set; }
        public List<OrderItem> Orders { get; set; }
        [DefaultDateTimeValue]
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdateAt { get; set; }        
    }
}