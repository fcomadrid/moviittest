﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviitTest.Models
{
    public class Ingredient
    {
        public Ingredient()
        {
            Sandwiches = new List<Sandwich>();
        }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual List<Sandwich> Sandwiches { get; set; }
    }
}