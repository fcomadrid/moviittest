﻿using MoviitTest.Machine;
using MoviitTest.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading;

namespace MoviitTest.CS
{
   
    public sealed class Orchestator
    {
        private static Orchestator instance = null;
        private static readonly object padlock = new object();
        private static bool _isActive = false;
        private readonly MainContext _db;        
        private Orchestator() { _db = new MainContext(); }

        public static Orchestator Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                        instance = new Orchestator();

                    return instance;
                }
            }
        }
        public void SearchOrders()
        {
            while (true)
            {
                if (_isActive)
                {
                    _isActive = false;
                    var order = _db.Order.Where(x => x.Status == "Confirmed").ToList();
                    if (order != null)
                    {                        
                        foreach (var o in order)
                        {
                            foreach (var oi in o.OrderItems)
                            {
                                string ingredients = string.Empty;
                                var s = _db.Sandwich.Include(x => x.Ingredients).First(x => x.Id == oi.Sandwich.Id);
                                ingredients = "[";
                                s.Ingredients.ForEach(x => ingredients += x.Id + ", ");
                                ingredients = ingredients.Trim().Substring(0, ingredients.Length - 2) + "]";
                                MachineResponse machineResponse = new Main().Process($"-cut {s.Cut}", $"-salsa {s.Salsa.Replace(" ", "")}", $"-compress {s.Compress}", $"-ingredients {ingredients.Trim()}");
                                machineResponse.OrderId = o.Id;
                                Config.WriteLog(UTILS.ParseJson.Serialize(machineResponse));
                            }
                            o.Status = "Ready";
                            _db.Entry(o).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                       
                    }                    
                }
                else
                {
                    Thread.Sleep(5000);
                }
            }
        }
        public void SetActive()
        {
            _isActive = true;
        }
    }
    
}