﻿using Newtonsoft.Json;

namespace MoviitTest.UTILS
{
    public class ParseJson
    {
        public static T Deserialize<T>(string dato)
        {
            return JsonConvert.DeserializeObject<T>(dato);
        }

        public static string Serialize<T>(T dato)
        {
            return JsonConvert.SerializeObject(dato);
        }
    }
}
