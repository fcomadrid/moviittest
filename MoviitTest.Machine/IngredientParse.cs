﻿using System.Collections.Generic;

namespace MoviitTest.Machine
{
    public class IngredientParse
    {
        public IngredientParse() {
            Ingredients = new List<IngredientObject>();
        }
        public List<IngredientObject> Ingredients { get; set; }
    }
    public class IngredientObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
