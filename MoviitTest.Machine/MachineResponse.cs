﻿
namespace MoviitTest.Machine
{
    public class MachineResponse
    {
        public int OrderId { get; set; }
        public string Cut { get; set; }
        public bool Compressed { get; set; }
        public string[] Content { get; set; }
    }
}
