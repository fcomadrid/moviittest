﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoviitTest.Machine
{
    public class Main
    {        
        private MachineResponse _machineResponse { get; set; }
        private bool _compress { get; set; }
        private string _ingredients { get; set; }
        private string _cut { get; set; }
        private string _salsa { get; set; }
        private List<string> _content { get; set; }
        public Main()
        {
            _content = new List<string>();
        }
        public MachineResponse Process(params string[] ps)
        {            
            foreach (var s in ps)
            {
                switch (s.Split(' ')[0])
                {
                    case "-cut":
                        _cut = s.Split(' ')[1];
                        break;
                    case "-salsa":
                        _salsa = s.Split(' ')[1];                        
                        break;
                    case "-compress":
                        _compress = bool.Parse(s.Split(' ')[1]);
                        break;
                    case "-ingredients":
                        _ingredients = s.Split(' ')[1].Replace("[", "").Replace("]", "");
                        break;
                }
            }                       
            _content.Add(AddBread());
            if (_salsa.Equals("OnTop"))
            {
                _content.Add(AddSalsa());
                if (!String.IsNullOrEmpty(_ingredients))
                    foreach (var i in _ingredients.Split(','))
                    {
                        _content.Add(AddIngredient(i));
                    }
            }
            else if (_salsa.Equals("Inside"))
            {
                int count = 0;
                foreach (var i in _ingredients.Split(','))
                {
                    _content.Add(AddIngredient(i));
                    if (count == 0)
                    {
                        _content.Add(_salsa);
                        count++;
                    }
                }
            }
            else {
                foreach (var i in _ingredients.Split(','))
                {
                    _content.Add(AddIngredient(i));   
                }
            };
            _content.Add(AddBread());
            Compress(_compress);
            CutSandwich(_cut);
            _machineResponse = new MachineResponse
            {
                Cut = _cut,
                Compressed = _compress,
                Content = _content.ToArray()
            };
            return _machineResponse;
        }

        private string AddBread()
        {
            return "bread";
        }
        private string AddIngredient(string ingredient)
        {
            Thread.Sleep(1000);
            return GetIngredient(int.Parse(ingredient));
        }
        private string AddSalsa()
        {
            Thread.Sleep(1000);
            return "salsa";
        }
        private void CutSandwich(string cut)
        {
            Thread.Sleep(1000);
        }
        private void Compress(bool compress)
        {
            if (compress)
            {
                Thread.Sleep(1000);
            }            
        }
        private string GetIngredient(int i)
        {
            
            return Config.Read(i).Name;
        }
    }
}
