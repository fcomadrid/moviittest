﻿using MoviitTest.UTILS;
using System.IO;

namespace MoviitTest.Machine
{
    public class Config
    {
        public static IngredientParse Read()
        {
            string reader = File.ReadAllText(string.Concat(Const.BasePath, "configMachine.json"));
            return ParseJson.Deserialize<IngredientParse>(reader);
        }
        public static IngredientObject Read(int i)
        {
            string reader = File.ReadAllText(string.Concat(Const.BasePath, "configMachine.json"));
            var ingredientParse =  ParseJson.Deserialize<IngredientParse>(reader);            
            return ingredientParse.Ingredients.Find(x => x.Id == i);
        }
        public static void Writer(IngredientParse ingredientParse)
        {
            using (StreamWriter streamWriter = new StreamWriter(string.Concat(Const.BasePath, "configMachine.json"))) {
                streamWriter.WriteLine(ParseJson.Serialize(ingredientParse));
            };
        }
        public static void WriteLog(string txt)
        {
            using (StreamWriter streamWriter = File.AppendText(string.Concat(Const.BasePath, "logMachine.txt")))
            {
                streamWriter.Write(txt);
            };
        }
    }
}
